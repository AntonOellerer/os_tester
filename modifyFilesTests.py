#! /usr/bin/python3

import os, filecmp, modifyFiles, time

path = './testfiles'
modifyFiles.path = path
modifyFiles.programname = 'mycompress'
permanentFiles = ['mycompress.c', 'mycompress.h']

def doTests():
    testGetFiles()
    testStaticRemove()

def testGetFiles():
    files = sorted(permanentFiles)
    recievedFiles = modifyFiles.getFiles()
    recievedFiles.sort()
    assert files == recievedFiles

def testStaticRemove():
    testC = os.path.join(path, 'testmycompress.c')
    testH = os.path.join(path, 'testmycompress.h')
    referenceC = os.path.join(path, 'mycompress.c.reference')
    referenceH = os.path.join(path, 'mycompress.h.reference')
    modifyFiles.createTests(permanentFiles)
    assert filecmp.cmp(testC, referenceC)
    assert filecmp.cmp(testH, referenceH)

def removeFiles():
    for filename in os.listdir(path):
        if filename.startswith('test'):
            fileToDelete = os.path.join(path, filename)
            os.unlink(fileToDelete)

try:
    doTests()
finally:
    removeFiles()
