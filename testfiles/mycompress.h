/**
 * @file mycompress.h
 * @author Anton Oellerer <aoellerer@gmail.com>
 * @date 15.10.2016
 *
 * @brief mycompress header file
 *
 * This headerfile handles the includes. 
 * */ 

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <features.h>
#include <unistd.h>
#include <getopt.h>
