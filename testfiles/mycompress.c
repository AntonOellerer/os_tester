/**
 * @file mycompress.c
 * @author Anton Oellerer <aoellerer@gmail.com>
 * @date 15.10.2016
 *
 * @brief Compression program.
 *
 * This program compresses a given string or file
 * and writes the output eiter to stdout or a specified file.
 */

#include "mycompress.h"

#define MAX_SIGNS 255	///< @brief Max allowed chars in one line.

bool readfromfiles;		///< @brief True if the text is to be read from a file.
bool writetofile;		///< @brief True if the compressed text should be written to a file.
int signsread;			///< @brief How many signs have been read.
int signswritten;		///< @brief How many signs have been written.
char *readfiles;		///< @brief Array of the filesnames to be read from.
char *writefile;		///< @brief Name of the file to be written to.

/**
 * @brief The function for retrieving the CLAs.
 * @details This function handles the command line arguments.
 * If the CLAs are invalid (not an argument, multiple occurence
 * it prints the usage and exits the program.
 * If the CLAs are valid, it sets the global variables.
 * global variables: readfromfiles, writetofiles, *readfiles, *writefile
 * @param argc The argument counter
 * @param argv The argument vector
 */
static void getargs(int argc, char * const argv[]);

/**
 * @brief Handles the processing of a single file.
 * @details Opens the specified line and passes 
 * every line to handleline.
 * @param file The file to be handled
 */
static void handlefile(char *file);

/**
 * @brief Handles the processing of one line.
 * @details Reads the line, passes it to compression
 * passes the compressed string to compressstring.
 * global variables: signsread, signswritten
 * @param line The line to be handled
 */
static void handleline(char *line);

/**
 * @brief Handles the compression of one string
 * @details Reads one char at the time, incrementing the occurence
 * counter by one if the char is the same as the last,
 * else prints the last char and its occurences to the string
 * and starting a new counter.
 * @param string The string to be compressed
 * @return The compressed String
 */
static char * compressstring(char *string);

/**
 * @brief Handles the writing of the string
 * @details If the string should be written to a file
 * it writes it to the specified file, else it writes it to stdout.
 * @param string The string to be written
 */
static void writestring(char *string);

/**
 * @brief Handles the writing of the stats
 * @details Calculates the read chars, the written chars
 * and the quota and writes it to the specified stream.
 * @param stream The stream to be written to
 * @param string The string to be written
 */
static void writestats(FILE *stream, char* string);

/**
 * @brief Prints the usage message to stdout
 * @details Prints the and the usage
 * instructions
 * @param programname The name of the program 
 * (argv[0])
 */
static void usage(char *programname);

/**
 * @brief Handles the flag
 * @details Checks whether the flag is valid
 * handles flags accordingly.
 * @param flag Flag to handle
 */
static void handleflag(char *flag);

/**
 * @brief Main function which handles subsequent calls
 * @details Gets the filenames, hands them to handlefile,
 * prints the usage statistic
 * @param argc The number of arguments (+1)
 * @param argv Tha arguments array
 */
int main (int argc, char **argv) {
		if(argc == 0){
				(void) usage(argv[0]);
		}
		(void) getargs(argc, argv);
		exit(EXIT_SUCCESS);
}

static void getargs(int argc, char *const argv[]) {
		int flag;
		int oocurrences = 0;
		while ((flag = getopt(argc, argv, "o:")) != -1) {
				switch (flag) {
						case 'o':
								writetofile = true;
								writefile = optarg;
								printf("%s\n", writefile);
								oocurrences += 1;
								break;
						default: /* '?' */
								(void) usage(argv[0]);
				}
		}
		if(oocurrences > 1) {
				usage(argv[0]);
		}
}

static void usage(char *programname) {
		fprintf(stderr, "Usage: %s [-o outfile] [infile1] [infile2] ... \n", programname);
		exit(EXIT_FAILURE);
}
