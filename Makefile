# example Makefile for testing
# Author: Anton Oellerer

CCOMPILER = gcc 
DEFS = -D_BSD_SOURCE -D_XOPEN_SOURCE=500
COMPILEFLAGS = -g -Wall -Wextra -pedantic -std=c99 -fsanitize=address  $(DEFS)
LINKFLAGS = -fsanitize=address

PROGRAMNAME = your programname 
CFILES = $(PROGRAMAME).c
OBJECTFILES = $(CFILES:.c=.o)
TESTNAME = your testname
SCRIPTPATH = path to pythonscript 
CFILESPATH = path to cfiles

.PHONY: all clean test

all: $(PROGRAMNAME)

$(PROGRAMNAME): $(OBJECTFILES) 
	$(CCOMPILER) $(LINKFLAGS) -o $@ $^

test: pre-test test-build 

pre-test:
	$(SCRIPTPATH) $(CFILESPATH) $(PROGRAMNAME)

test-build: $(TESTNAME) 

$(TESTNAME): $(TESTNAME).o 
	$(CCOMPILER) $(LINKFLAGS) -o $@ $^

%.o: %.c %.h
	$(CCOMPILER) $(COMPILEFLAGS) -c -o $@ $<

clean:
	rm -f $(PROGRAMNAME) $(TESTNAME) *.o test*.c test*.h
