Since the OS laboratory requires us to make all methods static it is quite difficult
to do TDD without any help, this framework tries to solve this.
It does this by copying the files and changing the static methods to non-static,
renaming the main and then it does the testing via a makefile.

Usage
modifyFiles.py <path to directory containing the c files>