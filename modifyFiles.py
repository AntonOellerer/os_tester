#! /usr/bin/python3

import os, sys, shutil

path = ''
programname = ''

def getFiles():
    files = []
    for currentFile in os.listdir(path):
        if currentFile == (programname + '.c') or currentFile == (programname + '.h'):
            files.append(currentFile)
    return files

def createTests(files):
    for currentFile in files:
        readPath = os.path.join(path, currentFile)
        writePath = os.path.join(path, 'test' + currentFile)
        readFile = open(readPath, 'r')
        writeFile = open(writePath, 'w')
        removeStatic(readFile, writeFile)
        readFile.close()
        writeFile.close()

def removeStatic(readFile, writeFile):
    for line in readFile:
        if line.startswith('static') and '=' not in line:
            line = line[7:]
        if line.startswith('int main'):
            line = line.replace('main', 'notmain')
        writeFile.write(line)

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: modifyFiles.py path programname')
        sys.exit()
    else:
        path = sys.argv[1]
        programname = sys.argv[2]
        path = os.path.abspath(path)
        files = getFiles()
        createTests(files)
